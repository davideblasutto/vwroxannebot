# Versione 1.1.0 (18/08/2018)

* Trasferito tutti i testi dei messaggi nel modulo `stringhe.js`
* Migliorie varie per utilizzo su Heroku

# Versione 1.0.0 (08/08/2018)

* Prima release funzionante
