// NLP estremamente semplice per individuare risposte positive/negative in italiano

const Sentiment = require('sentiment')

var sentiment = new Sentiment()

var opzioniSentiment = {
	extras: {
		'ok': 5,
		'si': 5,
		'sì': 5,
		'certo': 5
	}
}

module.exports = function(risposta) {

	const sentimentRisposta = sentiment.analyze(risposta, opzioniSentiment)

	return (sentimentRisposta.score > 0)

}
