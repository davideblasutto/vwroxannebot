module.exports = {
	utenteNonAutorizzato: 'Non sei autorizzato ad usare questo bot',
	start: 'Usa il comando /scarica oppure inviami direttamente un link a un brano di YouTube',
	inserisciChiaveRicerca: 'Scrivimi il nome della traccia, come se dovessi cercarla su YouTube',
	inizioRicercaYouTube: 'Cerco "$1"...',
	risultatoRicercaYouTube: 'Trovato *$1*. Lo scarico? \r\n ($2)',
	richiestaConfermaDownloadDiretto: 'Vuoi che scarichi questo brano? \r\n ($1)',
	erroreGenerico: 'Si è verificato un errore 🤷‍♂️‍, contatta Bla',
	inizioDownloadYouTube: 'Ok, lo scarico...',
	inizioUploadDropbox: 'Scaricato, ora lo carico su Dropbox...',
	uploadDropboxOk: 'Fatto, ora il brano è nella cartella su Dropbox 👍🏼',
	uploadDropboxErrore: 'C\'è stato un problema nel caricamento su Dropbox 😧',
	rifiuto: 'Va bene, non importa'
}
