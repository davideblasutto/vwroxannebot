// Moduli npm
const TelegramBot = require('node-telegram-bot-api')
const youtubeSearch = require('youtube-search-promise')
const youtubeDownloader = require('massive-ytmp3-downloader')
const tmp = require('tmp')
const fs = require('fs')
const path = require('path')
const Dropbox = require('dropbox').Dropbox
require('isomorphic-fetch')

// Moduli locali
const rispostaPositiva = require('./risposta-positiva.js')
const stringhe = require('./stringhe.js')
const ambiente = require('./ambiente.js')

// Avvia il bot
const bot = new TelegramBot(
	ambiente.tokenBotTelegram,
	{
		webHook: {
			port: process.env.PORT
		}
	}
)
console.log('Bot avviato')

// Gestione errori
bot.on('error', (errore) => {
	console.log('*** [Telegram] Errore generico:', errore)
})
bot.on('webhook_error', (errore) => {
	console.log('*** [Telegram] Errore webhook:', errore)
})

// Imposta webhook
const webhook = ambiente.urlHeroku + '/bot' + ambiente.tokenBotTelegram
bot.setWebHook(webhook)
console.log('Webhook impostato:', webhook)

// Ascolta i messaggi
console.log('In ascolto...')
bot.on('message', async (messaggio) => {

	try {

		// Controlla che l'utente sia autorizzato
		if (!ambiente.utentiAutorizzati.includes(messaggio.from.username)) {
			bot.sendMessage(messaggio.chat.id, stringa.utenteNonAutorizzato)
			return
		}

		console.log('Messaggio:', messaggio)

		// Comandi
		switch (messaggio.text.split(' ')[0]) {

		// *** 0. Comando /start o /help ***
		case '/start':
		case '/help':

			bot.sendMessage(messaggio.chat.id, stringhe.start)
			break

		// *** 1. Comando /scarica ***
		case '/scarica':

			bot.sendMessage(
				messaggio.chat.id,
				stringhe.inserisciChiaveRicerca,
				{
					reply_markup: {
						force_reply: true
					}
				}
			)

			break

		default:

			// Controlla se è una risposta
			if (messaggio.reply_to_message) {

				// Risposta a un messaggio precedente
				if (messaggio.reply_to_message.text == stringhe.inserisciChiaveRicerca) {

					// *** 2. Query a YouTube ***
					bot.sendMessage(
						messaggio.chat.id,
						stringhe.inizioRicercaYouTube
							.replace('$1', messaggio.text)
					)

					// Cerca su YouTube
					const risultatiRicerca = await youtubeSearch(
						messaggio.text,
						{
							maxResults: 1,
							key: ambiente.apiKeyYouTube
						}
					)

					console.log('---------')
					console.log(risultatiRicerca)

					const risultatoRicerca = risultatiRicerca[0]

					bot.sendMessage(
						messaggio.chat.id,
						stringhe.risultatoRicercaYouTube
							.replace('$1', risultatoRicerca.title)
							.replace('$2', risultatoRicerca.link),
						{
							parse_mode: 'Markdown',
							reply_markup: {
								force_reply: true
							}
						}
					)

				} else if (messaggio.reply_to_message.text.includes('youtube.com')) {

					// *** 3. Conferma/rifiuto ***
					if (rispostaPositiva(messaggio.text)) {

						// Conferma
						bot.sendMessage(messaggio.chat.id, stringhe.inizioDownloadYouTube)

						// A. Scarico da YouTube

						const destinazione = tmp.dirSync({ prefix: 'rx' })
						console.log('Cartella temporanea per il file scaricato:', destinazione)

						const risultatoDownload = await youtubeDownloader({
							apiKey: ambiente.apiKeyYouTube,
							outputPath: destinazione.name,
							verbose: true,
							searchTerms: [
								messaggio.reply_to_message.text.match(/\(([^)]+)\)/)[1] // Il link è tra parentesi
							]
						})
						console.log('File scaricato:', risultatoDownload)

						bot.sendMessage(messaggio.chat.id, stringhe.inizioUploadDropbox)

						// B. Carico su Dropbox

						const percorsoFileLocale = risultatoDownload[0].file
						console.log('Carico su Dropbox ' + percorsoFileLocale + '...')
						const dbx = new Dropbox({ accessToken: ambiente.accessTokenDropbox })
						const fileLocale = fs.readFileSync(percorsoFileLocale)
						const esitoUpload = await dbx.filesUpload({
							path: ambiente.idCartellaDropbox + '/' + path.basename(percorsoFileLocale),
							contents: fileLocale
						})
						console.log(esitoUpload)

						if (esitoUpload.id)
							bot.sendMessage(messaggio.chat.id, stringhe.uploadDropboxOk)
						else
							bot.sendMessage(messaggio.chat.id, stringhe.uploadDropboxErrore)

						// Fine

					} else {

						// Rifiuto
						bot.sendMessage(messaggio.chat.id, stringhe.rifiuto)

					}

				}

			} else {

				// Controlla se è un link a youtube
				if (messaggio.text.includes('youtube.com')) {

					// *** Z. Ricevuto link diretto ***
					bot.sendMessage(
						messaggio.chat.id,
						stringhe.richiestaConfermaDownloadDiretto
							.replace('$1', messaggio.text),
						{
							reply_markup: {
								force_reply: true
							}
						}
					)

				}

			}

			break

		}

	} catch(errore) {

		bot.sendMessage(
			messaggio.chat.id,
			stringhe.erroreGenerico
		)
		console.log('*** [Node.js] Errore:', errore)

	}

})
